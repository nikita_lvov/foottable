package com.example.test;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView score1, score2;
    private int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        score1 = findViewById(R.id.score1);
        score2 = findViewById(R.id.score2);
    }

    public void addScore1(View view) {
        counter++;
        score1.setText(String.valueOf(counter));
    }

    public void addScore2(View view) {
        counter++;
        score2.setText(String.valueOf(counter));
    }

    public void clearScores(View view) {
        counter = 0;
        score1.setText(String.valueOf(counter));
        score2.setText(String.valueOf(counter));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString("score1", score1.getText().toString());
        outState.putString("score2", score2.getText().toString());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        score1.setText(savedInstanceState.getString("score1"));
        score2.setText(savedInstanceState.getString("score2"));

        super.onRestoreInstanceState(savedInstanceState);
    }
}